//
//  ProfilePresenter.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import UIKit

class ProfilePresenter: ProfilePresenterProtocol {
    
    var interactor: ProfileInteractorProtocol?
    weak var view: ProfileViewControllerProtocol?
    var wireframe: ProfileWireframeProtocol?
    
    func getUserProfile() {
        self.interactor?.getUserProfile()
    }
    
    func displayUserProfileSuccess(withResponse response: UserProfileResponse) {
        self.view?.displayUserProfileSuccess(withResponse: response)
    }
    
    func displayUserProfileError(withError error: Error) {
        self.view?.displayUserProfileError(withError: error)
    }
    
    func uploadProfile(withImage image: UIImage) {
        self.interactor?.uploadProfile(withImage: image)
    }
    
    func uploadCover(withImage image: UIImage) {
        self.interactor?.uploadCover(withImage: image)
    }
    
    func navigateToChats(withView view: ProfileViewController) {
        self.wireframe?.navigateToChats(withView: view)
    }
    
    func navigateToGallery(withView view: ProfileViewController, withUser user: UserProfileResponse) {
        self.wireframe?.navigateToGallery(withView: view, withUser: user)
    }
    
    func navigateToEducation(withView view: ProfileViewController, withUser user: UserProfileResponse) {
        self.wireframe?.navigateToEducation(withView: view, withUser: user)
    }
    
    func navigateToCareer(withView view: ProfileViewController, withUser user: UserProfileResponse) {
        self.wireframe?.navigateToCareer(withView: view, withUser: user)
    }
    
}
