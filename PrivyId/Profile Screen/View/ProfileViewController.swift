//
//  ProfileViewController.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import UIKit
import Kingfisher

class ProfileViewController: UIViewController {
    
    var presenter: ProfilePresenterProtocol?
    var userData = ["Career", "Education", "Gallery", "Chats"]
    var isCover = false
    var isProfilePicture = false
    var coverUrlImage = ""
    var profileUrlImage = ""
    var coverImage: UIImage?
    var profileImage: UIImage?
    var user: UserProfileResponse?
    
    lazy var userTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
        
        return tableView
    }()
    
    let userProfileLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "User Profile"
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    
    let activityaIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.style = .medium
        indicator.isHidden = true
        
        return indicator
    }()
    
    let saveButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Save", for: .normal)
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(onClickSave), for: .touchUpInside)
        button.isEnabled = true
        
        return button
    }()
    
    let coverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .lightGray
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    let coverButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "camera"), for: .normal)
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(onClickCover), for: .touchUpInside)
        
        return button
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .darkGray
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    let profileButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "camera"), for: .normal)
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(onClickProfilePicture), for: .touchUpInside)
        
        return button
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .black
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDepedency()
        setupView()
        setupUserProfileService()
        setupTableView()
    }
    
    func setupDepedency() {
        ProfileWireframe.createMainModule(profileRef: self)
    }
    
    func setupUserProfileService() {
        self.presenter?.getUserProfile()
        self.activityaIndicator.isHidden = false
        self.activityaIndicator.startAnimating()
        self.coverButton.isEnabled = false
        self.profileButton.isEnabled = false
    }
    
    func setupView() {
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupTableView() {
        userTableView.register(UserCell.self, forCellReuseIdentifier: Constants.KEY_USER_CELL)
    }
    
    func setupInterfaceComponent() {
        view.addSubview(userProfileLabel)
        view.addSubview(activityaIndicator)
        view.addSubview(saveButton)
        view.addSubview(coverImageView)
        view.addSubview(coverButton)
        view.addSubview(profileImageView)
        view.addSubview(profileButton)
        view.addSubview(userTableView)
    }
    
    func setupConstraint() {
        
        userProfileLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        userProfileLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        activityaIndicator.leadingAnchor.constraint(equalTo: userProfileLabel.trailingAnchor, constant: 8).isActive = true
        activityaIndicator.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        
        saveButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        saveButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        
        coverImageView.topAnchor.constraint(equalTo: userProfileLabel.bottomAnchor, constant: 16).isActive = true
        coverImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        coverImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        coverImageView.heightAnchor.constraint(equalToConstant: view.frame.height * 0.25).isActive = true
        
        coverButton.centerXAnchor.constraint(equalTo: coverImageView.centerXAnchor).isActive = true
        coverButton.centerYAnchor.constraint(equalTo: coverImageView.centerYAnchor).isActive = true
        coverButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        coverButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        coverButton.layer.cornerRadius = 25
        
        profileImageView.centerYAnchor.constraint(equalTo: coverImageView.bottomAnchor).isActive = true
        profileImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: view.frame.width * 0.25).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: view.frame.width * 0.25).isActive = true
        profileImageView.layer.cornerRadius = (view.frame.width * 0.25) / 2
        
        profileButton.trailingAnchor.constraint(equalTo: profileImageView.trailingAnchor).isActive = true
        profileButton.topAnchor.constraint(equalTo: profileImageView.topAnchor).isActive = true
        profileButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        profileButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        profileButton.layer.cornerRadius = 15
        
        userTableView.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 36).isActive = true
        userTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        userTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        userTableView.heightAnchor.constraint(equalToConstant: 240).isActive = true
    }
    
    @objc func onClickSave() {
        
        if (profileUrlImage != "") {
            self.uploadProfile()
            return
        }
        
        if (coverUrlImage != "") {
            self.uploadCover()
            return
        }
        
    }
    
    func uploadProfile() {
        print("upload_profile", self.profileImage!)
        self.presenter?.uploadProfile(withImage: self.profileImage!)
    }
    
    func uploadCover() {
        print("upload_cover", self.coverImage!)
        self.presenter?.uploadCover(withImage: self.coverImage!)
    }
    
    func setupProfileImage(withUrl url: String) {
        let url = URL(string: url)
        let processor = DownsamplingImageProcessor(size: profileImageView.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 20)
        profileImageView.kf.indicatorType = .activity
        profileImageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
                self.profileButton.isEnabled = true
            case .failure( _):
                self.profileButton.isEnabled = true
                self.profileImageView.image = UIImage(named: "user")
            }
        }
    }
    
    func setupCoverImage(withUrl url: String) {
        let url = URL(string: url)
        let processor = DownsamplingImageProcessor(size: coverImageView.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 20)
        coverImageView.kf.indicatorType = .activity
        coverImageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success( _):
                self.coverButton.isEnabled = true
            case .failure( _):
                self.coverButton.isEnabled = true
                self.coverImageView.image = UIImage(systemName: "person")
            }
        }
    }
    
    @objc func onClickProfilePicture() {
        self.isProfilePicture = true
        openImagePicker()
    }
    
    @objc func onClickCover() {
        self.isCover = true
        openImagePicker()
    }
    
    func openImagePicker() {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { _ in
            self.resetStatus()
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    func resetStatus() {
        self.isCover = false
        self.isProfilePicture = false
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.resetStatus()
        }
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.resetStatus()
        }
    }
    
    func navigateToCareer() {
        self.presenter?.navigateToCareer(withView: self, withUser: self.user!)
    }
    
    func navigateToEducation() {
        
    }
    
    func navigateToChats() {
        
    }
    
    func navigateToGallery() {
        
    }
}

extension ProfileViewController: ProfileViewControllerProtocol {
    
    func displayUserProfileSuccess(withResponse response: UserProfileResponse) {
        self.activityaIndicator.isHidden = true
        self.activityaIndicator.stopAnimating()

        DispatchQueue.main.async {
            let profileURL = response.data?.user?.userPicture?.picture?.url ?? ""
            self.setupProfileImage(withUrl: profileURL)
            
            let coverUrl = response.data?.user?.coverPicture?.url ?? ""
            self.setupCoverImage(withUrl: coverUrl)
        }
        
        self.user = response
        
    }
    
    func displayUserProfileError(withError error: Error) {
        self.activityaIndicator.isHidden = true
        self.activityaIndicator.stopAnimating()
        
        let alert = UIAlertController(title: "System Error", message: "Something error with the server", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Refresh", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            self.setupUserProfileService()
                                        case .cancel:
                                            print("cancel")
                                        case .destructive:
                                            print("cancel")
                                        @unknown default: break
                                        }}))
        self.present(alert, animated: true, completion: nil)
    }
}

extension ProfileViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.KEY_USER_CELL, for: indexPath) as! UserCell
        cell.infoLabel.text = userData[indexPath.row]
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.user != nil {
            let user = userData[indexPath.row]
            
            if user == "Career" {
                navigateToCareer()
            } else if user == "Education" {
                navigateToEducation()
            } else if user == "Chats" {
                navigateToChats()
            } else {
                navigateToGallery()
            }
        }
       
    }

}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            
            let imageUrl = info[UIImagePickerController.InfoKey.imageURL] as! NSURL
            let stringImageUrl = imageUrl.absoluteString!
    
            if (isCover) {
                self.coverUrlImage = stringImageUrl.replacingOccurrences(of: "file://", with: "")
                print(coverUrlImage)
                self.coverImage = pickedImage
                self.coverImageView.image = coverImage
                self.isCover = false
            }
            if (isProfilePicture) {
                self.profileUrlImage = stringImageUrl.replacingOccurrences(of: "file://", with: "")
                print(profileUrlImage)
                self.profileImage = pickedImage
                self.profileImageView.image = profileImage
                self.isProfilePicture = false
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
