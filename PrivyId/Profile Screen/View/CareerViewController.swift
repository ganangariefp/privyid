//
//  CareerViewController.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import UIKit

class CareerViewController: UIViewController {
    
    var career: CareerUserModel? {
        didSet {
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .medium // 2-3
            
            self.companyTextField.text = career?.companyName ?? ""
            self.startTextField.text = career?.startingFrom ?? dateformatter.string(from: Date())
            self.endingTextField.text = career?.endingIn ?? dateformatter.string(from: Date())
        }
    }
    
    var startingDate: String?
    var endingDate: String?
    
    let backButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(onBack), for: .touchUpInside)
        button.setTitle("Back", for: .normal)
        
        return button
    }()
    
    let saveButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(onSave), for: .touchUpInside)
        button.setTitle("Save", for: .normal)
        
        return button
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Career"
        label.font = .boldSystemFont(ofSize: 24)
        
        return label
    }()
    
    let companyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Company Name"
        label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }()
    
    let startingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Starting from"
        label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }()
    
    let endingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Ending in"
        label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }()
    
    let companyTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.attributedPlaceholder = NSAttributedString(string: "Eg: xxx company", attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)])
        
        tf.autocorrectionType = UITextAutocorrectionType.no
        tf.keyboardType = UIKeyboardType.alphabet
        tf.returnKeyType = UIReturnKeyType.done
        tf.clearButtonMode = UITextField.ViewMode.whileEditing
        tf.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        tf.autocapitalizationType = .none
        tf.borderStyle = .none
        tf.backgroundColor = .clear
        
        return tf
    }()
    
    let underlineCompany: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
    }()
    
    let startTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.attributedPlaceholder = NSAttributedString(string: "Eg: 12 July 2010", attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)])
        
        tf.autocorrectionType = UITextAutocorrectionType.no
        tf.keyboardType = UIKeyboardType.alphabet
        tf.returnKeyType = UIReturnKeyType.done
        tf.clearButtonMode = UITextField.ViewMode.whileEditing
        tf.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        tf.autocapitalizationType = .none
        tf.borderStyle = .none
        tf.backgroundColor = .clear
        
        return tf
    }()
    
    let underlineStart: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
    }()
    
    let endingTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.attributedPlaceholder = NSAttributedString(string: "Eg: 12 July 2010", attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)])
        
        tf.autocorrectionType = UITextAutocorrectionType.no
        tf.keyboardType = UIKeyboardType.alphabet
        tf.returnKeyType = UIReturnKeyType.done
        tf.clearButtonMode = UITextField.ViewMode.whileEditing
        tf.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        tf.autocapitalizationType = .none
        tf.borderStyle = .none
        tf.backgroundColor = .clear
        
        return tf
    }()
    
    let underlineEnding: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = .black
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func setupView() {
        setupInterfaceComponent()
        setupConstraint()
        
        self.startTextField.setInputViewDatePicker(target: self, selector: #selector(tapStart))
        self.endingTextField.setInputViewDatePicker(target: self, selector: #selector(tapEnd))
    }
    
    @objc func onBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onSave() {
        let dateformatter = DateFormatter() // 2-2
        dateformatter.dateStyle = .medium // 2-3
        
        let params = [
            "position": "Staff",
            "company_name": companyTextField.text ?? "",
            "starting_from": startingDate ?? dateformatter.string(from: Date()),
            "ending_in": endingDate ?? dateformatter.string(from: Date())
        ] as [String : Any]
        
        print(LocalStorageService.shared.getAccessToken())
        UserProfileProvider.shared.postCareer(endpoint: .career, params: params) { (UserProfileResponse) in
            print(UserProfileResponse)
        } onError: { (Error) in
            print(Error)
        }

    }
    
    @objc func tapStart() {
        if let datePicker = self.startTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .medium // 2-3
            
            let text = dateformatter.string(from: datePicker.date)
            self.startingDate = text
            self.startTextField.text = text
        }
        self.startTextField.resignFirstResponder() // 2-5
    }
    
    @objc func tapEnd() {
        if let datePicker = self.endingTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .medium // 2-3
            
            let text = dateformatter.string(from: datePicker.date)
            self.endingDate = text
            self.endingTextField.text = text
        }
        self.endingTextField.resignFirstResponder() // 2-5
    }
    
    func setupInterfaceComponent() {
        view.addSubview(titleLabel)
        view.addSubview(backButton)
        view.addSubview(saveButton)
        
        view.addSubview(companyNameLabel)
        view.addSubview(companyTextField)
        view.addSubview(underlineCompany)
        
        view.addSubview(startingLabel)
        view.addSubview(startTextField)
        view.addSubview(underlineStart)
        
        view.addSubview(endingLabel)
        view.addSubview(endingTextField)
        view.addSubview(underlineEnding)
    }
    
    func setupConstraint() {
        titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        backButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        backButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 32).isActive = true
        
        saveButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        saveButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -32).isActive = true
        
        companyNameLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 40).isActive = true
        companyNameLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        
        companyTextField.topAnchor.constraint(equalTo: companyNameLabel.bottomAnchor, constant: 4).isActive = true
        companyTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        companyTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        companyTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        underlineCompany.bottomAnchor.constraint(equalTo: companyTextField.bottomAnchor, constant: -4).isActive = true
        underlineCompany.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        underlineCompany.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        underlineCompany.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        startingLabel.topAnchor.constraint(equalTo: underlineCompany.bottomAnchor, constant: 24).isActive = true
        startingLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        
        startTextField.topAnchor.constraint(equalTo: startingLabel.bottomAnchor, constant: 4).isActive = true
        startTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        startTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        startTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        underlineStart.bottomAnchor.constraint(equalTo: startTextField.bottomAnchor, constant: -4).isActive = true
        underlineStart.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        underlineStart.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        underlineStart.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        endingLabel.topAnchor.constraint(equalTo: underlineStart.bottomAnchor, constant: 24).isActive = true
        endingLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        
        endingTextField.topAnchor.constraint(equalTo: endingLabel.bottomAnchor, constant: 4).isActive = true
        endingTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        endingTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        endingTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        underlineEnding.bottomAnchor.constraint(equalTo: endingTextField.bottomAnchor, constant: -4).isActive = true
        underlineEnding.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        underlineEnding.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        underlineEnding.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

}
