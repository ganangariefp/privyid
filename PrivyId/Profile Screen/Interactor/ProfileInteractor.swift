//
//  ProfileInteractor.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import UIKit

class ProfileInteractor: ProfileInteractorProtocol {
    
    weak var presenter: ProfilePresenterProtocol?
    
    func getUserProfile() {
        UserProfileProvider.shared.getUserProfile(endpoint: .profileMe, params: nil) { (response) in
            self.presenter?.displayUserProfileSuccess(withResponse: response)
        } onError: { (error) in
            self.presenter?.displayUserProfileError(withError: error)
        }
    }
    
    func uploadCover(withImage image: UIImage) {
        UserProfileProvider.shared.postUserCoverImage(endpoint: .uploadsCover, image: image, params: nil) { (UserPictureResponse) in
            print(UserPictureResponse)
        } onError: { (Error) in
            print(Error)
        }

    }
    
    func uploadProfile(withImage image: UIImage) {
        UserProfileProvider.shared.postUserCoverImage(endpoint: .uploadsProfile, image: image, params: nil) { (UserPictureResponse) in
            print(UserPictureResponse)
        } onError: { (Error) in
            print(Error)
        }
    }
}
