//
//  ProfileProtocol.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import UIKit

protocol ProfileViewControllerProtocol: class {
    
    func displayUserProfileSuccess(withResponse response: UserProfileResponse)
    func displayUserProfileError(withError error: Error)
    
}

protocol ProfilePresenterProtocol: class {
    
    var interactor: ProfileInteractorProtocol? { get set }
    var view: ProfileViewControllerProtocol? { get set }
    var wireframe: ProfileWireframeProtocol? { get set }
    
    func getUserProfile()
    func displayUserProfileSuccess(withResponse response: UserProfileResponse)
    func displayUserProfileError(withError error: Error)
    
    func uploadProfile(withImage image: UIImage)
    func uploadCover(withImage image: UIImage)
    
    func navigateToCareer(withView view: ProfileViewController, withUser user: UserProfileResponse)
    func navigateToEducation(withView view: ProfileViewController, withUser user: UserProfileResponse)
    func navigateToChats(withView view: ProfileViewController)
    func navigateToGallery(withView view: ProfileViewController, withUser user: UserProfileResponse)
    
}

protocol ProfileInteractorProtocol: class {
    
    var presenter: ProfilePresenterProtocol? { get set }
    
    func getUserProfile()
    
    func uploadProfile(withImage image: UIImage)
    func uploadCover(withImage image: UIImage)
}

protocol ProfileWireframeProtocol: class {
    func navigateToCareer(withView view: ProfileViewController, withUser user: UserProfileResponse)
    func navigateToEducation(withView view: ProfileViewController, withUser user: UserProfileResponse)
    func navigateToChats(withView view: ProfileViewController)
    func navigateToGallery(withView view: ProfileViewController, withUser user: UserProfileResponse)
}
