//
//  UserPictureResponse.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import Foundation

struct UserPictureResponse: Codable {
    let data: UserPictureModel?
}
