//
//  UserProfileResponse.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import UIKit

struct UserProfileResponse: Codable {
    let data: DataUserProfile?
}

struct DataUserProfile: Codable {
    let user: UserProfileModel?
}

struct UserProfileModel: Codable {
    let id: String?
    let name: String?
    let level: Int?
    let age: Int?
    let birthday: String?
    let gender: Int?
    let zodiac: String?
    let hometown: String?
    let bio: String?
    let latlong: String?
    let education: EducationUserModel?
    let career: CareerUserModel?
    let userPictures: [UserPictureModel]?
    let userPicture: UserPictureModel?
    let coverPicture: PictureModel?
}

struct EducationUserModel: Codable {
    let schoolName: String?
    let graduationTime: String?
}

struct CareerUserModel: Codable {
    let companyName: String?
    let startingFrom: String?
    let endingIn: String?
}

struct UserPictureModel: Codable {
    let id: String?
    let picture: PictureModel?
}

struct PictureModel: Codable {
    let url: String?
}

//{
//  "data": {
//    "user": {
//      "id": "SG49580",
//      "name": "SG49580",
//      "level": 1,
//      "age": null,
//      "birthday": null,
//      "gender": null,
//      "zodiac": null,
//      "hometown": null,
//      "bio": null,
//      "latlong": null,
//      "education": {
//        "school_name": null,
//        "graduation_time": null
//      },
//      "career": {
//        "company_name": null,
//        "starting_from": null,
//        "ending_in": null
//      },
//      "user_pictures": [],
//      "user_picture": null,
//      "cover_picture": {
//        "url": null
//      }
//    }
//  }
//}
