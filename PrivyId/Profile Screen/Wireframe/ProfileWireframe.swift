//
//  ProfileWireframe.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import UIKit

class ProfileWireframe: ProfileWireframeProtocol {
    
    class func createMainModule(profileRef: ProfileViewController) {
        let presenter: ProfilePresenterProtocol = ProfilePresenter()
        
        profileRef.presenter = presenter
        profileRef.presenter?.wireframe = ProfileWireframe()
        profileRef.presenter?.view = profileRef
        profileRef.presenter?.interactor = ProfileInteractor()
        profileRef.presenter?.interactor?.presenter = presenter
    }
    
    func navigateToCareer(withView view: ProfileViewController, withUser user: UserProfileResponse) {
        let careerVC = CareerViewController()
        careerVC.career = user.data?.user?.career
        
        view.navigationController?.pushViewController(careerVC, animated: true)
    }
    
    func navigateToEducation(withView view: ProfileViewController,  withUser user: UserProfileResponse) {
//        let userProfileVC = UserProfileViewController()
//        userProfileVC.userId = userId
//
//        view.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    
    func navigateToGallery(withView view: ProfileViewController,  withUser user: UserProfileResponse) {
//        let userProfileVC = UserProfileViewController()
//        userProfileVC.userId = userId
//
//        view.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    
    func navigateToChats(withView view: ProfileViewController) {
//        let userProfileVC = UserProfileViewController()
//        userProfileVC.userId = userId
//        
//        view.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    
}


