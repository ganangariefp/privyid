//
//  ResponseAccessUser.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

struct UserAccessModel: Codable {
    let accessToken: String?
    let tokenType: String?
}

struct DataAccessModel: Codable {
    let user: UserAccessModel?
    
    private enum CodingKeys: String, CodingKey {
        case user
    }
}

struct RegisterAccessUser: Codable {
    let data: DataAccessModel?
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
}
