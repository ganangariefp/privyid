//
//  OTPInteractor.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import UIKit

class OTPInteractor: OTPInteractorProtocol {
    
    weak var presenter: OTPPresenterProtocol?
    
    func requestOTP(withParams params: [String: Any]) {
        RegisterProvider.shared.postRequestOTP(endpoint: .otpRequest, params: params) { (RegisterUserResponse) in
            self.presenter?.displayRequestOTPSuccess(withResponse: RegisterUserResponse)
        } onError: { (Error) in
            self.presenter?.displayRequestOTPError(withError: Error)
        }
    }
    
    func matchOTP(withParams params: [String : Any]) {
        RegisterProvider.shared.postMatchOTP(endpoint: .otpMatch, params: params) { (RegisterAccessUser) in
            self.presenter?.displayMatchOTPSuccess(withResponse: RegisterAccessUser)
        } onError: { (Error) in
            self.presenter?.displayMatchOTPError(withError: Error)
        }

    }
}
