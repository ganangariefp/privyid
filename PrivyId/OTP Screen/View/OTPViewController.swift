//
//  OTPViewController.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import UIKit

class OTPViewController: UIViewController {
    
    var presenter: OTPPresenterProtocol?
    
    let verifyingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Verifying"
        label.font = .boldSystemFont(ofSize: 20)
        label.isHidden = true
        
        return label
    }()
    
    let activityaIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.style = .large
        indicator.isHidden = true
        
        return indicator
    }()
    
    let codeTextField: OTPTextField = {
        let textField = OTPTextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        return textField
    }()
    
    let otpLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "OTP Verification"
        label.font = .boldSystemFont(ofSize: 24)
        
        return label
    }()
    
    let informationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Enter the code here"
        label.font = .boldSystemFont(ofSize: 20)
        
        return label
    }()
    
    let resendButton: UIButton = {
        let yourAttributes: [NSAttributedString.Key: Any] = [
              .font: UIFont.systemFont(ofSize: 20),
              .foregroundColor: UIColor.white,
              .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributeString = NSMutableAttributedString(string: "Resend OTP", attributes: yourAttributes)

        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setAttributedTitle(attributeString, for: .normal)
        button.addTarget(self, action: #selector(requestOTP), for: .touchUpInside)
        
        return button
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .black
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDepedency()
        setupOTPTextField()
        setupView()
        requestOTP()
    }
    
    func setupDepedency() {
        OTPWireframe.createMainModule(otpRef: self)
    }
    
    func setupView() {
        setupInterfaceComponent()
        setupConstraint()
    }
    
    @objc func requestOTP() {
        let phone = LocalStorageService.shared.getPhoneNumber()
//        let phone = "6287822614967"
        let params = ["phone": phone]
        
        self.presenter?.requestOTP(withParams: params)
    }
    
    func setupInterfaceComponent() {
        view.addSubview(otpLabel)
        view.addSubview(codeTextField)
        view.addSubview(informationLabel)
        view.addSubview(resendButton)
        view.addSubview(verifyingLabel)
        view.addSubview(activityaIndicator)
    }
    
    func setupConstraint() {
        otpLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 72).isActive = true
        otpLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        informationLabel.topAnchor.constraint(equalTo: otpLabel.bottomAnchor, constant: 40).isActive = true
        informationLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 52).isActive = true
        
        codeTextField.topAnchor.constraint(equalTo: informationLabel.bottomAnchor, constant: 8).isActive = true
        codeTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        codeTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        codeTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        codeTextField.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        resendButton.topAnchor.constraint(equalTo: codeTextField.bottomAnchor, constant: 40).isActive = true
        resendButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        verifyingLabel.topAnchor.constraint(equalTo: codeTextField.bottomAnchor, constant: 40).isActive = true
        verifyingLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -12).isActive = true
        
        activityaIndicator.topAnchor.constraint(equalTo: codeTextField.bottomAnchor, constant: 40).isActive = true
        activityaIndicator.leadingAnchor.constraint(equalTo: verifyingLabel.trailingAnchor, constant: 12).isActive = true
    }
    
    func setupOTPTextField() {
        codeTextField.configure()
        codeTextField.didEnterLastDigit = { [weak self] code in
            let userID = LocalStorageService.shared.getUserID()
            let otpCode = code
            let params = ["user_id": userID, "otp_code": otpCode]
            
            self?.presenter?.matchOTP(withParams: params)
            self?.showLoadingIndicator()
        }
    }
    
    func showLoadingIndicator() {
        codeTextField.isEnabled = false
        resendButton.isHidden = true
        verifyingLabel.isHidden = false
        activityaIndicator.isHidden = false
        activityaIndicator.startAnimating()
    }
    
    func enableStatus() {
        codeTextField.isEnabled = true
        resendButton.isHidden = false
        verifyingLabel.isHidden = true
        activityaIndicator.isHidden = true
        activityaIndicator.stopAnimating()
    }
}

extension OTPViewController: OTPViewControllerProtocol {
    
    func displayRequestOTPSuccess(withResponse response: RegisterUserResponse) {
        LocalStorageService.shared.setUserID(userID: response.data?.user?.id ?? "")
    }
    
    func displayRequestOTPError(withError error: Error) {
        let alert = UIAlertController(title: "Warning", message: "Failed get OTP code, please try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("cancel")
                                        case .cancel:
                                            print("cancel")
                                        case .destructive:
                                            print("cancel")
                                        @unknown default: break
                                        }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayMatchOTPSuccess(withResponse response: RegisterAccessUser) {
        self.enableStatus()
        LocalStorageService.shared.setAccessToken(withAccessToken: response.data?.user?.accessToken ?? "", withKey: "accessToken")
        self.presenter?.navigateToMain()
    }
    
    func displayMatchOTPError(withError error: Error) {
        self.enableStatus()
    }

}
