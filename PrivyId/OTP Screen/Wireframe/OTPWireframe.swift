//
//  OTPWireframe.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import UIKit

class OTPWireframe: OTPWireframeProtocol {
    
    class func createMainModule(otpRef: OTPViewController) {
        let presenter: OTPPresenterProtocol = OTPPresenter()
        
        otpRef.presenter = presenter
        otpRef.presenter?.wireframe = OTPWireframe()
        otpRef.presenter?.view = otpRef
        otpRef.presenter?.interactor = OTPInteractor()
        otpRef.presenter?.interactor?.presenter = presenter
    }
    
    func navigateToMain() {
        let navController: UINavigationController = UINavigationController()
        navController.viewControllers = [MainViewController()]
        UIApplication.shared.windows.first?.rootViewController = navController
    }
    
}
