//
//  OTPPresenter.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import UIKit

class OTPPresenter: OTPPresenterProtocol {
    
    var interactor: OTPInteractorProtocol?
    weak var view: OTPViewControllerProtocol?
    var wireframe: OTPWireframeProtocol?
    
    func requestOTP(withParams params: [String: Any]) {
        self.interactor?.requestOTP(withParams: params)
    }
    
    func displayRequestOTPSuccess(withResponse response: RegisterUserResponse) {
        self.view?.displayRequestOTPSuccess(withResponse: response)
    }
    
    func displayRequestOTPError(withError error: Error) {
        self.view?.displayRequestOTPError(withError: error)
    }
    
    func matchOTP(withParams params: [String : Any]) {
        self.interactor?.matchOTP(withParams: params)
    }
    
    func displayMatchOTPSuccess(withResponse response: RegisterAccessUser) {
        self.view?.displayMatchOTPSuccess(withResponse: response)
    }
    
    func displayMatchOTPError(withError error: Error) {
        self.view?.displayMatchOTPError(withError: error)
    }
    
    func navigateToMain() {
        self.wireframe?.navigateToMain()
    }
}
