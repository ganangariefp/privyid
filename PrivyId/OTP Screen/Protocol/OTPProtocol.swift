//
//  OTPProtocol.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import UIKit

protocol OTPViewControllerProtocol: class {
    
    func displayRequestOTPSuccess(withResponse response: RegisterUserResponse)
    func displayRequestOTPError(withError error: Error)
    
    func displayMatchOTPSuccess(withResponse response: RegisterAccessUser)
    func displayMatchOTPError(withError error: Error)
}

protocol OTPPresenterProtocol: class {
    
    var interactor: OTPInteractorProtocol? { get set }
    var view: OTPViewControllerProtocol? { get set }
    var wireframe: OTPWireframeProtocol? { get set }
    
    func requestOTP(withParams params: [String: Any])
    func matchOTP(withParams params: [String: Any])
    func displayRequestOTPSuccess(withResponse response: RegisterUserResponse)
    func displayRequestOTPError(withError error: Error)
    
    func displayMatchOTPSuccess(withResponse response: RegisterAccessUser)
    func displayMatchOTPError(withError error: Error)
    
    func navigateToMain()
}

protocol OTPInteractorProtocol: class {
    
    var presenter: OTPPresenterProtocol? { get set }
    
    func requestOTP(withParams params: [String: Any])
    func matchOTP(withParams params: [String: Any])
}

protocol OTPWireframeProtocol: class {
    func navigateToMain()
}
