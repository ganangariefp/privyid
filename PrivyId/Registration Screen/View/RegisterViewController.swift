//
//  ViewController.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 08/02/21.
//

import UIKit

class RegisterViewController: UIViewController {
    
    var presenter: RegisterPresenterProtocol?
    var countries = ["Indonesia", "Zimbabwe", "Italy"]
    
    lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.dataSource = self
        picker.delegate = self
        picker.backgroundColor = .clear
        picker.isHidden = true
        
        return picker
    }()
    
    let registrationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "PrivyID"
        label.font = .boldSystemFont(ofSize: 20)
        
        return label
    }()
    
    let phoneLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]

        let firstString = NSMutableAttributedString(string: "Phone Number ", attributes: firstAttributes)
        let secondString = NSAttributedString(string: "*", attributes: secondAttributes)

        firstString.append(secondString)

        // set attributed text on a UILabel
        label.attributedText = firstString
        
        return label
    }()
    
    let countryCodeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(" +62", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setImage(UIImage(named: "indonesia"), for: .normal)
        button.backgroundColor = .white
        
        return button
    }()
    
    let phoneNumberTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.attributedPlaceholder = NSAttributedString(string: "87923456887", attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)])
        
        tf.autocorrectionType = UITextAutocorrectionType.no
        tf.keyboardType = UIKeyboardType.phonePad
        tf.returnKeyType = UIReturnKeyType.done
        tf.clearButtonMode = UITextField.ViewMode.whileEditing
        tf.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        tf.autocapitalizationType = .none
        tf.borderStyle = .none
        tf.backgroundColor = .clear
        
        return tf
    }()
    
    let underlinePhone: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
    }()
    
    let passwordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]

        let firstString = NSMutableAttributedString(string: "Password ", attributes: firstAttributes)
        let secondString = NSAttributedString(string: "*", attributes: secondAttributes)

        firstString.append(secondString)

        // set attributed text on a UILabel
        label.attributedText = firstString
        
        return label
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.attributedPlaceholder = NSAttributedString(string: "***********", attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)])
        
        tf.autocorrectionType = UITextAutocorrectionType.no
        tf.keyboardType = UIKeyboardType.namePhonePad
        tf.returnKeyType = UIReturnKeyType.done
        tf.clearButtonMode = UITextField.ViewMode.whileEditing
        tf.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        tf.autocapitalizationType = .none
        tf.borderStyle = .none
        tf.backgroundColor = .clear
        tf.isSecureTextEntry = true
        
        return tf
    }()
    
    let underlinePassword: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
    }()
    
    let countryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]

        let firstString = NSMutableAttributedString(string: "Country ", attributes: firstAttributes)
        let secondString = NSAttributedString(string: "*", attributes: secondAttributes)

        firstString.append(secondString)

        // set attributed text on a UILabel
        label.attributedText = firstString
        
        return label
    }()
    
    let countryButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "indonesia"), for: .normal)
        button.backgroundColor = .white
        
        return button
    }()
    
    let countryNameButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Indonesia", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .clear
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        button.addTarget(self, action: #selector(chooseCountry), for: .touchUpInside)
        
        return button
    }()
    
    let underlineCountry: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
    }()
    
    let registerButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Register", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(displayP3Red: 52/255 ,green:  116/255, blue: 235/255, alpha: 1)
        button.addTarget(self, action: #selector(registerUser), for: .touchUpInside)
        
        return button
    }()
    
    let navigateToLoginLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Have an account?"
        label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }()
    
    let loginButton: UIButton = {
        let yourAttributes: [NSAttributedString.Key: Any] = [
              .font: UIFont.systemFont(ofSize: 14),
              .foregroundColor: UIColor.systemBlue,
              .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributeString = NSMutableAttributedString(string: "Login", attributes: yourAttributes)

        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setAttributedTitle(attributeString, for: .normal)
        button.addTarget(self, action: #selector(navigateToLogin), for: .touchUpInside)
        
        return button
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .black
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupView()
        setupDepedency()
        hideKeyboardWhenTappedAround()
        
    }
    
    func setupDepedency() {
        RegisterWireframe.createMainModule(registerRef: self)
    }
    
    func setupView() {
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupInterfaceComponent() {
        view.addSubview(registrationLabel)
        view.addSubview(phoneLabel)
        view.addSubview(countryCodeButton)
        view.addSubview(phoneNumberTextField)
        view.addSubview(underlinePhone)
        
        view.addSubview(passwordLabel)
        view.addSubview(passwordTextField)
        view.addSubview(underlinePassword)
        
        view.addSubview(countryLabel)
        view.addSubview(countryButton)
        view.addSubview(countryNameButton)
        view.addSubview(underlineCountry)
        
        view.addSubview(registerButton)
        view.addSubview(navigateToLoginLabel)
        view.addSubview(loginButton)
        
        view.addSubview(pickerView)
    }
    
    func setupConstraint() {
        registrationLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        registrationLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 140).isActive = true
        
        phoneLabel.topAnchor.constraint(equalTo: registrationLabel.bottomAnchor, constant: 36).isActive = true
        phoneLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        
        countryCodeButton.topAnchor.constraint(equalTo: phoneLabel.bottomAnchor, constant: 16).isActive = true
        countryCodeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        countryCodeButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        countryCodeButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        countryCodeButton.layer.cornerRadius = 8
        
        phoneNumberTextField.leadingAnchor.constraint(equalTo: countryCodeButton.trailingAnchor, constant: 8).isActive = true
        phoneNumberTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        phoneNumberTextField.topAnchor.constraint(equalTo: phoneLabel.bottomAnchor, constant: 16).isActive = true
        phoneNumberTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        underlinePhone.bottomAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor, constant: -4).isActive = true
        underlinePhone.leadingAnchor.constraint(equalTo: countryCodeButton.trailingAnchor, constant: 8).isActive = true
        underlinePhone.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        underlinePhone.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        passwordLabel.topAnchor.constraint(equalTo: countryCodeButton.bottomAnchor, constant: 16).isActive = true
        passwordLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        
        passwordTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        passwordTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: 8).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        underlinePassword.bottomAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: -4).isActive = true
        underlinePassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        underlinePassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        underlinePassword.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        countryLabel.topAnchor.constraint(equalTo: underlinePassword.bottomAnchor, constant: 16).isActive = true
        countryLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        
        countryButton.topAnchor.constraint(equalTo: countryLabel.bottomAnchor, constant: 16).isActive = true
        countryButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        countryButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        countryButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        countryButton.layer.cornerRadius = 8
        
        countryNameButton.leadingAnchor.constraint(equalTo: countryButton.trailingAnchor, constant: 8).isActive = true
        countryNameButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        countryNameButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        countryNameButton.topAnchor.constraint(equalTo: countryLabel.bottomAnchor, constant: 8).isActive = true
        
        underlineCountry.bottomAnchor.constraint(equalTo: countryNameButton.bottomAnchor).isActive = true
        underlineCountry.leadingAnchor.constraint(equalTo: countryButton.trailingAnchor, constant: 8).isActive = true
        underlineCountry.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        underlineCountry.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        registerButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        registerButton.topAnchor.constraint(equalTo: underlineCountry.bottomAnchor, constant: 40).isActive = true
        registerButton.layer.cornerRadius = 12
        
        navigateToLoginLabel.topAnchor.constraint(equalTo: registerButton.bottomAnchor, constant: 16).isActive = true
        navigateToLoginLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -16).isActive = true
        
        loginButton.centerYAnchor.constraint(equalTo: navigateToLoginLabel.centerYAnchor, constant: -2).isActive = true
        loginButton.leadingAnchor.constraint(equalTo: navigateToLoginLabel.trailingAnchor, constant: 4).isActive = true
        
        pickerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        pickerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        pickerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    @objc func registerUser() {
        disableStatus()
        let phoneNumber = "62" + phoneNumberTextField.text!
        let password = passwordTextField.text!
        let country = countryNameButton.currentTitle!
        
        let params = [
            "phone": phoneNumber,
            "password": password,
            "country": country,
            "latlong": "1",
            "device_token": LocalStorageService.shared.getDeviceToken(),
            "device_type": "0"
        ] as [String : Any]
        
        presenter?.registerUser(withParams: params)
    }
    
    func disableStatus() {
        countryCodeButton.isEnabled = false
        phoneNumberTextField.isEnabled = false
        passwordTextField.isEnabled = false
        countryButton.isEnabled = false
        countryNameButton.isEnabled = false
        registerButton.isEnabled = false
        registerButton.setTitle("", for: .normal)
        registerButton.loadingIndicator(show: true)
    }
    
    func enableStatus() {
        countryCodeButton.isEnabled = true
        phoneNumberTextField.isEnabled = true
        passwordTextField.isEnabled = true
        countryButton.isEnabled = true
        countryNameButton.isEnabled = true
        registerButton.isEnabled = true
        registerButton.setTitle("Register", for: .normal)
        registerButton.loadingIndicator(show: false)
    }
    
    func showErrorAlert() {
        let alert = UIAlertController(title: "Warning", message: "Phone has already been taken", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("cancel")
                                        case .cancel:
                                            print("cancel")
                                        case .destructive:
                                            print("cancel")
                                        @unknown default: break
                                        }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func chooseCountry() {
        pickerView.isHidden = false
    }
    
    @objc func navigateToLogin() {
        self.presenter?.navigateToLogin(withView: self)
    }

}

extension RegisterViewController: RegisterViewControllerProtocol {
    func displayRegisterUserSuccess(withResponse registerResponse: RegisterUserResponse) {
        self.enableStatus()
        LocalStorageService.shared.setUserID(userID: registerResponse.data?.user?.id ?? "")
        LocalStorageService.shared.setPhoneNumber(phoneNumber: registerResponse.data?.user?.phone ?? "")
        
        self.presenter?.navigateToOTP()
    }
    
    func displayRegisterUserError(withError error: Error) {
        self.enableStatus()
        self.showErrorAlert()
    }
}

extension RegisterViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickerView.isHidden = true
        self.countryNameButton.setTitle(countries[row], for: .normal)
    }
}
