//
//  RegisterUserResponse.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 08/02/21.
//

import Foundation

struct UserDeviceModel: Codable {
    let deviceToken: String?
    let deviceType: String?
    let deviceStatus: String?
}

struct UserModel: Codable {
    let id: String?
    let phone: String?
    let userStatus: String?
    let userType: String?
    let sugarId: String?
    let country: String?
    let latlong: String?
    let userDevice: UserDeviceModel?
}

struct DataModel: Codable {
    let user: UserModel?
    
    private enum CodingKeys: String, CodingKey {
        case user
    }
}

struct RegisterUserResponse: Codable {
    let data: DataModel?
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
}

//{
//  "data": {
//    "user": {
//      "id": "2d5485e9-b36d-44f5-8216-e0147f9232f9",
//      "phone": "6287822614966",
//      "user_status": "pending",
//      "user_type": "normal_user",
//      "sugar_id": "SG79546",
//      "country": "Indonesia",
//      "latlong": null,
//      "user_device": {
//        "device_token": "3bd18ed1d3a77382dab0ee5d77441a34449657946e79af7d0b41f41f6bd932f1",
//        "device_type": "ios",
//        "device_status": "grant"
//      }
//    }
//  }
//}
