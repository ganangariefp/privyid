//
//  RegisterProtocol.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 08/02/21.
//

import UIKit

protocol RegisterViewControllerProtocol: class {
    func displayRegisterUserSuccess(withResponse registerResponse: RegisterUserResponse)
    func displayRegisterUserError(withError error: Error)
}

protocol RegisterPresenterProtocol: class {
    var interactor: RegisterInteractorProtocol? { get set }
    var view: RegisterViewControllerProtocol? { get set }
    var wireframe: RegisterWireframeProtocol? { get set }
    
    func registerUser(withParams params: [String: Any])
    func displayRegisterUserSuccess(withResponse registerResponse: RegisterUserResponse)
    func displayRegisterUserError(withError error: Error)
    
    func navigateToOTP()
    func navigateToLogin(withView view: RegisterViewController)
}

protocol RegisterInteractorProtocol: class {
    var presenter: RegisterPresenterProtocol? { get set }
    
    func registerUser(withParams params: [String: Any])
}

protocol RegisterWireframeProtocol: class {
    func navigateToOTP()
    func navigateToLogin(withView view: RegisterViewController)
}
