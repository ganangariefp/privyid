//
//  RegisterWirefram.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 08/02/21.
//

import UIKit

class RegisterWireframe: RegisterWireframeProtocol {
    
    class func createMainModule(registerRef: RegisterViewController) {
        let presenter: RegisterPresenterProtocol = RegisterPresenter()
        
        registerRef.presenter = presenter
        registerRef.presenter?.wireframe = RegisterWireframe()
        registerRef.presenter?.view = registerRef
        registerRef.presenter?.interactor = RegisterInteractor()
        registerRef.presenter?.interactor?.presenter = presenter
    }
    
    func navigateToOTP() {
        let navController: UINavigationController = UINavigationController()
        navController.viewControllers = [OTPViewController()]
        UIApplication.shared.windows.first?.rootViewController = navController
    }
    
    func navigateToLogin(withView view: RegisterViewController) {
        view.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
}
