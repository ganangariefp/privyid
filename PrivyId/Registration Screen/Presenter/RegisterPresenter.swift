//
//  RegisterPresenter.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 08/02/21.
//

import UIKit

class RegisterPresenter: RegisterPresenterProtocol {
   
    var interactor: RegisterInteractorProtocol?
    weak var view: RegisterViewControllerProtocol?
    var wireframe: RegisterWireframeProtocol?
    
    func registerUser(withParams params: [String : Any]) {
        interactor?.registerUser(withParams: params)
    }
    
    func displayRegisterUserSuccess(withResponse registerResponse: RegisterUserResponse) {
        self.view?.displayRegisterUserSuccess(withResponse: registerResponse)
    }
    
    func displayRegisterUserError(withError error: Error) {
        self.view?.displayRegisterUserError(withError: error)
    }
    
    func navigateToOTP() {
        self.wireframe?.navigateToOTP()
    }
    
    func navigateToLogin(withView view: RegisterViewController) {
        self.wireframe?.navigateToLogin(withView: view)
    }
    
}
