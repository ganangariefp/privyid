//
//  RegisterInteractor.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 08/02/21.
//

import UIKit

class RegisterInteractor: RegisterInteractorProtocol {
    
    weak var presenter: RegisterPresenterProtocol?
    
    func registerUser(withParams params: [String : Any]) {
        RegisterProvider.shared.postRegisterUser(endpoint: .register, params: params) { (RegisterUserResponse) in
            self.presenter?.displayRegisterUserSuccess(withResponse: RegisterUserResponse)
        } onError: { (Error) in
            self.presenter?.displayRegisterUserError(withError: Error)
        }

    }
}
