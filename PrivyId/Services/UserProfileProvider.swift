//
//  UserProfileProvider.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import UIKit
import Alamofire

public class UserProfileProvider: UserProfileService {
    
    public static let shared = UserProfileProvider()
    private init() {}
    
    // get api key and base url from user defined setting
    private let baseUrl = BaseURL.shared
    
    private let urlSession = URLSession.shared
    
    private let jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        jsonDecoder.dateDecodingStrategy = .formatted(dateFormatter)
        return jsonDecoder
    }()
    
    func getUserProfile(endpoint: UserProfileEndpoint, params: [String : Any]?, onSuccess: @escaping (UserProfileResponse) -> Void, onError: @escaping (Error) -> Void) {
        self.handeRequest(requestedObjectType: UserProfileResponse.self, method: "GET", endpoint: "\(endpoint.rawValue)", params: params, onSuccess: onSuccess, onError: onError)
    }
    
    func postUserProfileImage(endpoint: UserProfileEndpoint, image: UIImage?, params: [String: String]?, onSuccess: @escaping (UserPictureResponse) -> Void, onError: @escaping (Error) -> Void) {
        self.handeUploadRequest(requestedObjectType: UserPictureResponse.self, method: "POST", endpoint: "\(endpoint.rawValue)", image: image, params: params, onSuccess: onSuccess, onError: onError)
    }
    
    func postUserCoverImage(endpoint: UserProfileEndpoint, image: UIImage?, params: [String: String]?, onSuccess: @escaping (UserPictureResponse) -> Void, onError: @escaping (Error) -> Void) {
        self.handeUploadImage(requestedObjectType: UserPictureResponse.self, method: "POST", endpoint: "\(endpoint.rawValue)", image: image, params: params, onSuccess: onSuccess, onError: onError)
    }
    
    func postCareer(endpoint: UserProfileEndpoint, params: [String : Any]?, onSuccess: @escaping (UserProfileResponse) -> Void, onError: @escaping (Error) -> Void) {
        self.handeRequest(requestedObjectType: UserProfileResponse.self, method: "POST", endpoint: "\(endpoint.rawValue)", params: params, onSuccess: onSuccess, onError: onError)
    }
    
    // function for handling api Request with generic type, so it can always be used later
    private func handeRequest<T:Codable>(requestedObjectType:T.Type, method: String, endpoint: String, params: [String: Any]?, onSuccess: @escaping (T) -> Void, onError: @escaping (Error) -> Void){
        
        guard var urlComponents = URLComponents(string: "\(self.baseUrl)\(endpoint)") else {
            onError(UserProfileError.invalidEndpoint)
            return
        }
        
        if params != nil {
            var queryItems = [URLQueryItem]()
            if let params = params {
                queryItems.append(contentsOf: params.map { URLQueryItem(name: $0.key, value: $0.value as? String) })
            }

            urlComponents.queryItems = queryItems
        }
        
        guard let url = urlComponents.url else {
            onError(UserProfileError.invalidEndpoint)
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue( "Bearer \(LocalStorageService.shared.getAccessToken())", forHTTPHeaderField: "Authorization")
        
        urlSession.dataTask(with: request) { (data, response, error) in
            if error != nil {
                self.errorHandler(onErrorCallback: onError, error: UserProfileError.errorFromApi)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, 200..<300 ~= httpResponse.statusCode else {
                self.errorHandler(onErrorCallback: onError, error: UserProfileError.invalidResponse)
                return
            }
            
            guard let data = data else {
                self.errorHandler(onErrorCallback: onError, error: UserProfileError.noData)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let response = try decoder.decode(T.self, from: data)
                DispatchQueue.main.async {
                    onSuccess(response)
                }
            } catch let DecodingError.dataCorrupted(context) {
                print(context)
            } catch let DecodingError.keyNotFound(key, context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.valueNotFound(value, context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.typeMismatch(type, context)  {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                print("error: ", error)
            }
        }.resume()
    }
    
    private func handeUploadImage<T:Codable>(requestedObjectType:T.Type, method: String, endpoint: String, image: UIImage?, params: [String: String]?, onSuccess: @escaping (T) -> Void, onError: @escaping (Error) -> Void) {
        
        guard var urlComponents = URLComponents(string: "\(self.baseUrl)\(endpoint)") else {
            onError(UserProfileError.invalidEndpoint)
            return
        }
        
        if params != nil {
            var queryItems = [URLQueryItem]()
            if let params = params {
                queryItems.append(contentsOf: params.map { URLQueryItem(name: $0.key, value: $0.value) })
            }

            urlComponents.queryItems = queryItems
        }
        
        guard let url = urlComponents.url else {
            onError(UserProfileError.invalidEndpoint)
            return
        }
        
        guard let data = image?.jpegData(compressionQuality: 0.9) else {
            return
        }
        
        
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(data, withName: "cover", fileName: "cover.jpg", mimeType: "image/jpg")
        }, to: url, headers: ["Authorization": "Bearer \(LocalStorageService.shared.getAccessToken())"]).response { response in
            
            let error = response.error
            
            if error != nil {
                self.errorHandler(onErrorCallback: onError, error: UserProfileError.errorFromApi)
                return
            }
            
            guard let httpResponse = response.response, 200..<300 ~= httpResponse.statusCode else {
                self.errorHandler(onErrorCallback: onError, error: UserProfileError.invalidResponse)
                return
            }
            
            guard let data = response.data else {
                self.errorHandler(onErrorCallback: onError, error: UserProfileError.noData)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let response = try decoder.decode(T.self, from: data)
                DispatchQueue.main.async {
                    onSuccess(response)
                }
            } catch let DecodingError.dataCorrupted(context) {
                print(context)
            } catch let DecodingError.keyNotFound(key, context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.valueNotFound(value, context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.typeMismatch(type, context)  {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                print("error: ", error)
            }
        }.resume()
        
    }
    
    private func handeUploadRequest<T:Codable>(requestedObjectType:T.Type, method: String, endpoint: String, image: UIImage?, params: [String: String]?, onSuccess: @escaping (T) -> Void, onError: @escaping (Error) -> Void){
        
        guard var urlComponents = URLComponents(string: "\(self.baseUrl)\(endpoint)") else {
            onError(UserProfileError.invalidEndpoint)
            return
        }
        
        if params != nil {
            var queryItems = [URLQueryItem]()
            if let params = params {
                queryItems.append(contentsOf: params.map { URLQueryItem(name: $0.key, value: $0.value) })
            }

            urlComponents.queryItems = queryItems
        }
        
        guard let url = urlComponents.url else {
            onError(UserProfileError.invalidEndpoint)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(LocalStorageService.shared.getAccessToken())", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let imageData = image?.jpegData(compressionQuality: 1)
        if imageData == nil  {
            return
        }
        
        request.httpBody = createBodyWithParameters(parameters: nil, filePathKey: "file", imageDataKey: imageData! as NSData, boundary: boundary, imgKey: "image") as Data
        
        urlSession.dataTask(with: request) { (data, response, error) in
            print("RES",response)
            print("ERROR",error)
            print("DATA",data)
            
            if error != nil {
                self.errorHandler(onErrorCallback: onError, error: UserProfileError.errorFromApi)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, 200..<300 ~= httpResponse.statusCode else {
                let responssss = response as? HTTPURLResponse
                print(responssss?.statusCode as Any)
                self.errorHandler(onErrorCallback: onError, error: UserProfileError.invalidResponse)
                return
            }
            
            guard let data = data else {
                self.errorHandler(onErrorCallback: onError, error: UserProfileError.noData)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let response = try decoder.decode(T.self, from: data)
                DispatchQueue.main.async {
                    onSuccess(response)
                }
            } catch let DecodingError.dataCorrupted(context) {
                print(context)
            } catch let DecodingError.keyNotFound(key, context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.valueNotFound(value, context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.typeMismatch(type, context)  {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                print("error: ", error)
            }
        }.resume()
    }
    
    // handle error here
    private func errorHandler(onErrorCallback: @escaping(_ error: Error) -> Void, error: Error) {
        DispatchQueue.main.async {
            onErrorCallback(error)
        }
    }
    
}

func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String, imgKey: String) -> NSData {
    let body = NSMutableData();
    
    if parameters != nil {
        for (key, value) in parameters! {
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString(string: "\(value)\r\n")
        }
    }
    
    let filename = "\(imgKey).jpg"
    let mimetype = "image/jpg"
    
    body.appendString(string: "--\(boundary)\r\n")
    body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
    body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
    body.append(imageDataKey as Data)
    body.appendString(string: "\r\n")
    body.appendString(string: "--\(boundary)--\r\n")
    
    return body
}

func generateBoundaryString() -> String {
    return "Boundary-\(NSUUID().uuidString)"
}



extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
