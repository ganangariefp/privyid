//
//  RegisterProvider.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 08/02/21.
//

import UIKit

public class RegisterProvider: RegisterService {
    
    public static let shared = RegisterProvider()
    private init() {}
    
    // get api key and base url from user defined setting
    private let baseUrl = BaseURL.shared
    
    private let urlSession = URLSession.shared
    
    private let jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        jsonDecoder.dateDecodingStrategy = .formatted(dateFormatter)
        return jsonDecoder
    }()
    
    func postRegisterUser(endpoint: RegisterEndpoint, params: [String : Any]?, onSuccess: @escaping (RegisterUserResponse) -> Void, onError: @escaping (Error) -> Void) {
        self.handeRequest(requestedObjectType: RegisterUserResponse.self, endpoint: "\(endpoint.rawValue)", params: params, onSuccess: onSuccess, onError: onError)
    }
    
    func postRequestOTP(endpoint: RegisterEndpoint, params: [String : Any]?, onSuccess: @escaping (RegisterUserResponse) -> Void, onError: @escaping (Error) -> Void) {
        self.handeRequest(requestedObjectType: RegisterUserResponse.self, endpoint: "\(endpoint.rawValue)", params: params, onSuccess: onSuccess, onError: onError)
    }
    
    func postMatchOTP(endpoint: RegisterEndpoint, params: [String : Any]?, onSuccess: @escaping (RegisterAccessUser) -> Void, onError: @escaping (Error) -> Void) {
        self.handeRequest(requestedObjectType: RegisterAccessUser.self, endpoint: "\(endpoint.rawValue)", params: params, onSuccess: onSuccess, onError: onError)
    }
    
    // function for handling api Request with generic type, so it can always be used later
    private func handeRequest<T:Codable>(requestedObjectType:T.Type, endpoint: String, params: [String : Any]?, onSuccess: @escaping (T) -> Void, onError: @escaping (Error) -> Void){
        
        guard var urlComponents = URLComponents(string: "\(self.baseUrl)\(endpoint)") else {
            onError(RegisterError.invalidEndpoint)
            return
        }
        
        var queryItems = [URLQueryItem]()
        if let params = params {
            queryItems.append(contentsOf: params.map { URLQueryItem(name: $0.key, value: $0.value as? String) })
        }
        
        urlComponents.queryItems = queryItems
        
        guard let url = urlComponents.url else {
            onError(RegisterError.invalidEndpoint)
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        urlSession.dataTask(with: request) { (data, response, error) in
            if error != nil {
                self.errorHandler(onErrorCallback: onError, error: RegisterError.errorFromApi)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, 200..<300 ~= httpResponse.statusCode else {
                self.errorHandler(onErrorCallback: onError, error: RegisterError.invalidResponse)
                return
            }
            
            guard let data = data else {
                self.errorHandler(onErrorCallback: onError, error: RegisterError.noData)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let response = try decoder.decode(T.self, from: data)
                DispatchQueue.main.async {
                    onSuccess(response)
                }
            } catch let DecodingError.dataCorrupted(context) {
                print(context)
            } catch let DecodingError.keyNotFound(key, context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.valueNotFound(value, context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.typeMismatch(type, context)  {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                print("error: ", error)
            }
        }.resume()
    }
    
    // handle error here
    private func errorHandler(onErrorCallback: @escaping(_ error: Error) -> Void, error: Error) {
        DispatchQueue.main.async {
            onErrorCallback(error)
        }
    }
    
}


