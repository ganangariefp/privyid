//
//  LoginService.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import Foundation

// define available service
protocol LoginService {
    
    // post login user
    func postLoginUser(endpoint: LoginEndpoint , params: [String: Any]?, onSuccess: @escaping (_ response: RegisterUserResponse) -> Void, onError: @escaping(_ error: Error) -> Void)
}

// list of available login endpoint
public enum LoginEndpoint: String, CaseIterable, Identifiable {
    public var id: String { self.rawValue }
    
    case login = "oauth/sign_in"
    case revoke = "oauth/revoke"
}

// define custom error
public enum LoginError: Error {
    case errorFromApi
    case noData
    case invalidEndpoint
    case invalidResponse
    case serializationError
}
