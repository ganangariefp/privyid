//
//  RegisterService.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 08/02/21.
//

import Foundation

// define available service
protocol RegisterService {
    
    // post register user
    func postRegisterUser(endpoint: RegisterEndpoint , params: [String: Any]?, onSuccess: @escaping (_ response: RegisterUserResponse) -> Void, onError: @escaping(_ error: Error) -> Void)
    
    func postRequestOTP(endpoint: RegisterEndpoint , params: [String: Any]?, onSuccess: @escaping (_ response: RegisterUserResponse) -> Void, onError: @escaping(_ error: Error) -> Void)
    
    func postMatchOTP(endpoint: RegisterEndpoint , params: [String: Any]?, onSuccess: @escaping (_ response: RegisterAccessUser) -> Void, onError: @escaping(_ error: Error) -> Void)
}

// list of available register endpoint
public enum RegisterEndpoint: String, CaseIterable, Identifiable {
    public var id: String { self.rawValue }
    
    case register = "register"
    case remove = "register/remove"
    case otpMatch = "register/otp/match"
    case otpRequest = "register/otp/request"
}

// define custom error
public enum RegisterError: Error {
    case errorFromApi
    case noData
    case invalidEndpoint
    case invalidResponse
    case serializationError
}
