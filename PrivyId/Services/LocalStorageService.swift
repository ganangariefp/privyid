//
//  LocalStorageService.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 08/02/21.
//

import Foundation

class LocalStorageService {
    
    static let shared = LocalStorageService()
    
    // USER DEFAULT
    let defaults = UserDefaults.standard
    
    // get userID on local storage
    func getUserID() -> String {
        
        guard let userID = defaults.string(forKey: "userID") else {
            return ""
        }
        
        return userID
    }
    
    // set userID
    func setUserID(userID: String) {
        defaults.set(userID, forKey: "userID")
        defaults.synchronize()
    }
    
    func cleanUserID() {
        defaults.set(nil, forKey: "userID")
        defaults.synchronize()
    }
    
    // get phone number on local storage
    func getPhoneNumber() -> String {
        
        guard let phoneNumber = defaults.string(forKey: "phoneNumber") else {
            return ""
        }
        
        return phoneNumber
    }
    
    // set phone number
    func setPhoneNumber(phoneNumber: String) {
        defaults.set(phoneNumber, forKey: "phoneNumber")
        defaults.synchronize()
    }
    
    func cleanPhoneNumber() {
        defaults.set(nil, forKey: "phoneNumber")
        defaults.synchronize()
    }
    
    func setDeviceToken(withDeviceToken deviceToken: String, withKey key: String) {
        KeychainWrapper.standard.set(deviceToken, forKey: key)
    }
    
    func getDeviceToken() -> String {
        let deviceToken = KeychainWrapper.standard.string(forKey: "deviceToken")
        
        return deviceToken ?? ""
    }
    
    func setAccessToken(withAccessToken accessToken: String, withKey key: String) {
        KeychainWrapper.standard.set(accessToken, forKey: key)
    }
    
    func getAccessToken() -> String {
        let accessToken = KeychainWrapper.standard.string(forKey: "accessToken")
        
        return accessToken ?? ""
    }
    
    func removeAccessToken() {
        KeychainWrapper.standard.set("", forKey: "accessToken")
    }
}
