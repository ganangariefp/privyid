//
//  UserProfileService.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 10/02/21.
//

import UIKit

// define available service
protocol UserProfileService {
    
    // get user profile
    func getUserProfile(endpoint: UserProfileEndpoint , params: [String: Any]?, onSuccess: @escaping (_ response: UserProfileResponse) -> Void, onError: @escaping(_ error: Error) -> Void)
    
    func postUserProfileImage(endpoint: UserProfileEndpoint , image: UIImage?, params: [String: String]?, onSuccess: @escaping (_ response: UserPictureResponse) -> Void, onError: @escaping(_ error: Error) -> Void)
    
    func postUserCoverImage(endpoint: UserProfileEndpoint , image: UIImage?, params: [String: String]?, onSuccess: @escaping (_ response: UserPictureResponse) -> Void, onError: @escaping(_ error: Error) -> Void)
    
    func postCareer(endpoint: UserProfileEndpoint, params: [String: Any]?, onSuccess: @escaping (_ response: UserProfileResponse) -> Void, onError: @escaping(_ error: Error) -> Void)
    
}

// list of available stock endpoint
public enum UserProfileEndpoint: String, CaseIterable, Identifiable {
    public var id: String { self.rawValue }
    
    case profileMe = "profile/me"
    case profile = "profile"
    case career = "profile/career"
    case education = "profile/education"
    case uploadsProfile = "uploads/profile"
    case uploadsCover = "uploads/cover"
}

// define custom error
public enum UserProfileError: Error {
    case errorFromApi
    case noData
    case invalidEndpoint
    case invalidResponse
    case serializationError
}
