//
//  LoginViewController.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import UIKit

class LoginViewController: UIViewController {
    
    var presenter: LoginPresenterProtocol?
    
    let loginLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Login"
        label.font = .boldSystemFont(ofSize: 20)
        
        return label
    }()
    
    let phoneLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]

        let firstString = NSMutableAttributedString(string: "Phone Number ", attributes: firstAttributes)
        let secondString = NSAttributedString(string: "*", attributes: secondAttributes)

        firstString.append(secondString)

        // set attributed text on a UILabel
        label.attributedText = firstString
        
        return label
    }()
    
    let phoneNumberTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.attributedPlaceholder = NSAttributedString(string: "62 87923456887", attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)])
        
        tf.autocorrectionType = UITextAutocorrectionType.no
        tf.keyboardType = UIKeyboardType.phonePad
        tf.returnKeyType = UIReturnKeyType.done
        tf.clearButtonMode = UITextField.ViewMode.whileEditing
        tf.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        tf.autocapitalizationType = .none
        tf.borderStyle = .none
        tf.backgroundColor = .clear
        
        return tf
    }()
    
    let underlinePhone: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
    }()
    
    let passwordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]

        let firstString = NSMutableAttributedString(string: "Password ", attributes: firstAttributes)
        let secondString = NSAttributedString(string: "*", attributes: secondAttributes)

        firstString.append(secondString)

        // set attributed text on a UILabel
        label.attributedText = firstString
        
        return label
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.attributedPlaceholder = NSAttributedString(string: "***********", attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)])
        
        tf.autocorrectionType = UITextAutocorrectionType.no
        tf.keyboardType = UIKeyboardType.namePhonePad
        tf.returnKeyType = UIReturnKeyType.done
        tf.clearButtonMode = UITextField.ViewMode.whileEditing
        tf.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        tf.autocapitalizationType = .none
        tf.borderStyle = .none
        tf.backgroundColor = .clear
        tf.isSecureTextEntry = true
        
        return tf
    }()
    
    let underlinePassword: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
    }()
    
    let loginButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Login", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(displayP3Red: 52/255 ,green:  116/255, blue: 235/255, alpha: 1)
        button.addTarget(self, action: #selector(loginUser), for: .touchUpInside)
        
        return button
    }()
    
    let navigateToRegisterLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Do not have an account?"
        label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }()
    
    let registerButton: UIButton = {
        let yourAttributes: [NSAttributedString.Key: Any] = [
              .font: UIFont.systemFont(ofSize: 14),
              .foregroundColor: UIColor.systemBlue,
              .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributeString = NSMutableAttributedString(string: "Register", attributes: yourAttributes)

        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setAttributedTitle(attributeString, for: .normal)
        button.addTarget(self, action: #selector(navigateToRegister), for: .touchUpInside)
        
        return button
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .black
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDepedency()
        setupView()
        hideKeyboardWhenTappedAround()
    }
    
    func setupDepedency() {
        LoginWireframe.createMainModule(loginRef: self)
    }
    
    func setupView() {
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupInterfaceComponent() {
        view.addSubview(loginLabel)
        view.addSubview(phoneLabel)
        view.addSubview(phoneNumberTextField)
        view.addSubview(underlinePhone)
        
        view.addSubview(passwordLabel)
        view.addSubview(passwordTextField)
        view.addSubview(underlinePassword)
        
        view.addSubview(loginButton)
        view.addSubview(navigateToRegisterLabel)
        view.addSubview(registerButton)
    }
    
    func setupConstraint() {
        loginLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 140).isActive = true
        loginLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        phoneLabel.topAnchor.constraint(equalTo: loginLabel.bottomAnchor, constant: 48).isActive = true
        phoneLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 36).isActive = true
        
        phoneNumberTextField.topAnchor.constraint(equalTo: phoneLabel.bottomAnchor, constant: 8).isActive = true
        phoneNumberTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 36).isActive = true
        phoneNumberTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        phoneNumberTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        underlinePhone.bottomAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor).isActive = true
        underlinePhone.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 36).isActive = true
        underlinePhone.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        underlinePhone.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        passwordLabel.topAnchor.constraint(equalTo: underlinePhone.bottomAnchor, constant: 24).isActive = true
        passwordLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 36).isActive = true
        
        passwordTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 36).isActive = true
        passwordTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: 8).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        underlinePassword.bottomAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        underlinePassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 36).isActive = true
        underlinePassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36).isActive = true
        underlinePassword.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        loginButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        loginButton.topAnchor.constraint(equalTo: underlinePassword.bottomAnchor, constant: 40).isActive = true
        loginButton.layer.cornerRadius = 12
        
        navigateToRegisterLabel.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 16).isActive = true
        navigateToRegisterLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -16).isActive = true
        
        registerButton.centerYAnchor.constraint(equalTo: navigateToRegisterLabel.centerYAnchor, constant: -2).isActive = true
        registerButton.leadingAnchor.constraint(equalTo: navigateToRegisterLabel.trailingAnchor, constant: 4).isActive = true
    }
    
    @objc func loginUser() {
        disableStatus()
        let phoneNumber = phoneNumberTextField.text!
        let password = passwordTextField.text!
        
        let params = [
            "phone": phoneNumber,
            "password": password,
            "latlong": "1",
            "device_token": LocalStorageService.shared.getDeviceToken(),
            "device_type": "0"
        ] as [String : Any]
        
        // LUPA PASSWORDNYA LOL
//        presenter?.loginUser(withParams: params)
        
        let alert = UIAlertController(title: "Warning", message: "Sorry i forgot the password", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("cancel")
                                        case .cancel:
                                            print("cancel")
                                        case .destructive:
                                            print("cancel")
                                        @unknown default: break
                                        }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func navigateToRegister() {
        navigationController?.popViewController(animated: true)
    }
    
    func disableStatus() {
        phoneNumberTextField.isEnabled = false
        passwordTextField.isEnabled = false
        loginButton.setTitle("", for: .normal)
        loginButton.loadingIndicator(show: true)
    }
    
}

extension LoginViewController: LoginViewControllerProtocol {
    
}
