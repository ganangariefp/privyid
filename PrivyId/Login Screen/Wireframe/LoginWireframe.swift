//
//  LoginWireframe.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import UIKit

class LoginWireframe: LoginWireframeProtocol {
    
    class func createMainModule(loginRef: LoginViewController) {
        let presenter: LoginPresenterProtocol = LoginPresenter()
        
        loginRef.presenter = presenter
        loginRef.presenter?.wireframe = LoginWireframe()
        loginRef.presenter?.view = loginRef
        loginRef.presenter?.interactor = LoginInteractor()
        loginRef.presenter?.interactor?.presenter = presenter
    }
    
}
