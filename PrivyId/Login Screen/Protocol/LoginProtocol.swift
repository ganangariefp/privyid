//
//  LoginProtocol.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import UIKit

protocol LoginViewControllerProtocol: class {

}

protocol LoginPresenterProtocol: class {
    
    var interactor: LoginInteractorProtocol? { get set }
    var view: LoginViewControllerProtocol? { get set }
    var wireframe: LoginWireframeProtocol? { get set }

}

protocol LoginInteractorProtocol: class {
    
    var presenter: LoginPresenterProtocol? { get set }
    
}

protocol LoginWireframeProtocol: class {
   
}
