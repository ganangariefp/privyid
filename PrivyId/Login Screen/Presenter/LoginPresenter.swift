//
//  LoginPresenter.swift
//  PrivyId
//
//  Created by Ganang Arief Pratama on 09/02/21.
//

import UIKit

class LoginPresenter: LoginPresenterProtocol {
    
    var interactor: LoginInteractorProtocol?
    weak var view: LoginViewControllerProtocol?
    var wireframe: LoginWireframeProtocol?
    
}
